import React from "react";
import Header from "./Component/Header";
import "./App.css";
import Game from "./Component/game/Game";
import indiaFlag from "../src/img/IndiaFlag.png";
import ukFlag from "../src/img/UKFlag.png";
import scottishFlag from "../src/img/ScottishFlag.png";
import mexicanFlag from "../src/img/MexicanFlag.png";
class App extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Header />

        <Game
          venue1="Walmart Parking Lot Stadium"
          name="Cesar"
          name2="Greg"
          logo2={mexicanFlag}
          logo={scottishFlag}
        />
        <Game
          venue2="Above Ground Pool Depot"
          name="Andre"
          name2="Raj"
          logo2={ukFlag}
          logo={indiaFlag}
        />
      </React.Fragment>
    );
  }
}
export default App;
