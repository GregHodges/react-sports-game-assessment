
import React from 'react'
import Team from '../team/Team'
import soccerField16 from "./soccerField16.png";




function Game(props) {
  return (
    <React.Fragment>
      <div className="gameContainer">
        <div className="main">
          <div className="header">"Not-so" Premier League-</div>
          <div className="venue">{props.venue1} </div>
          <div className="venue">{props.venue2} </div>
        </div>
        <div className = "scoreBoardMain">
          {/* <button onClick className="reset">
            Reset Game
          </button> */}
        </div>
        <div className="scoreBoard">
          <Team logo={props.logo} name={props.name} />
          <div className="versus">VS</div>
          <Team logo={props.logo2} name={props.name2} />
        </div>
        <img className="field" src={soccerField16} alt=""></img>
      </div>
    </React.Fragment>
  );
}

export default Game