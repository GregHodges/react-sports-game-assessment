

import React from 'react'

import happy from '../Audio/happy.wav' 
import sad from "../Audio/sad.wav";

 class Team extends React.Component {
   state = {
     count: 0,
     goal: 0,
   };

   happy = new Audio(happy);
   sad = new Audio(sad);

   playMiss = () => {
     const sound = this.sad;
     sound.play();
   };

   playScore = () => {
     const sound = this.happy;
     sound.play();
   };

   metric = () => {
     return Math.round((this.state.goal / this.state.count) * 100) + "%";
   };

   kickBall = (props) => {
     let point = Math.ceil(Math.random() * 5);
     if (point === 5) {
       this.setState((state) => {
         // this.metric();
         this.playScore();
         return {
           ...state,
           goal: state.goal + 1,
           count: state.count + 1,
         };
       });
     }
     if (point !== 5) {
       this.setState((state, props) => {
         this.playMiss();
         return {
           ...state,
           count: state.count + 1,
         };
       });
     }
   };

   render() {
     const { count } = this.state;
     const { goal } = this.state;
     return (
       <React.Fragment>
         <div className="info">
           <p>{this.props.name}</p>
           <img className="logo" src={this.props.logo} alt="logo"></img>
           <button onClick={this.kickBall}>Kick Ball</button>
           <p className="Shots">Shots Taken {count}</p>

           {count > 0 && (
             <React.Fragment>
               <div className="Metric">
                 <p>Shot Metric {this.metric()}</p>
               </div>
               <div className="score">
                 <p>Goals {goal}</p>
               </div>
             </React.Fragment>
           )}
         </div>
       </React.Fragment>
     );
   }
 }

export default Team